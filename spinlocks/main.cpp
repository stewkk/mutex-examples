#include "tas_spinlock.hpp"
#include "ticket_spinlock.hpp"

#include <twist/rt/run.hpp>
#include <twist/test/race.hpp>
#include <twist/test/plate.hpp>

// std::lock_guard
#include <mutex>
#include <iostream>

template <class SpinLock>
void Test(size_t threads) {
  twist::rt::Run([threads] {
    SpinLock spinlock;
    twist::test::Plate plate;  // Guarded by spinlock

    twist::test::Race race;

    for (size_t i = 0; i < threads; ++i) {
      race.Add([&]() {
        for (size_t i = 0; i < 100'500; ++i) {
          spinlock.Lock();
          {
            // Critical section
            plate.Access();
          }
          spinlock.Unlock();
        }
      });
    };

    race.Run();

    std::cout << "Critical sections: " << plate.AccessCount() << std::endl;
  });
}

int main() {
  Test<TASSpinLock>(/*threads=*/5);
  Test<TicketLock>(/*threads=*/5);
  return 0;
}
