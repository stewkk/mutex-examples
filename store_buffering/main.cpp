#include <iostream>
#include <thread>

// Prevents compiler reordering
inline void CompilerBarrier() {
  asm volatile("" ::: "memory");
}

// T1: x = 1; r1 = y;
// T2: y = 1; r2 = x;

void MaybeStoreBuffering(size_t iter) {
  // Shared variables
  int x = 0;
  int y = 0;

  // Local variables (registers)
  int r1 = 0;  // t1
  int r2 = 0;  // t2

  std::thread t1([&] {
    x = 1;
    CompilerBarrier();
    r1 = y;
  });

  std::thread t2([&] {
    y = 1;
    CompilerBarrier();
    r2 = x;
  });

  t1.join();
  t2.join();

  if (r1 == 0 && r2 == 0) {
    std::cout << "Iteration #" << iter << ": CPU is broken =(" << std::endl;
    std::abort();
  }
}

int main() {
  for (size_t i = 0; ; ++i) {
    MaybeStoreBuffering(i);
    if (i % 10000 == 0) {
      std::cout << "Iterations made: " << i + 1 << std::endl;
    }
  }

  return 0;
}
